# Report Builder

A simple markdown to pdf builder using pandoc.

## Requirements

Install podman (alternatively you can use docker).

## Usage

Create a folder called `in` and another folder called `out` it will be share as a volume on your container.

Put your `markdown` file with your external ressources in the `in` folder.

To create the container image.
`$ make build`

To run the image.
`$ make run REPORT=<report_name.md>`

You will be prompt to give some information such as :

- `title`
- `subtitle`
- `author`
- `date`

The output pdf file can be found in the `out` directory.

## Sources

This project is inspired by the [project](https://github.com/noraj/OSCP-Exam-Report-Template-Markdown) of noraj.
