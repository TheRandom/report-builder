.PHONY: build run

build:
	podman build -f Dockerfile -t report-builder:latest .

run: build
	mkdir -p in
	mkdir -p out
	podman run --rm -it -e LOGLEVEL=${LOGLEVEL} -e REPORT=${REPORT} -v ./in:/in -v ./out:/out report-builder:latest
