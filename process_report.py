#!/usr/bin/env python3

import os
import logging

PATH_CONFIG = "config.yaml"
IS_CONFIG_FILE = os.path.isfile(PATH_CONFIG)

LOGLEVEL=os.getenv('LOGLEVEL', 'INFO').upper()
if LOGLEVEL == '':
    LOGLEVEL = 'INFO'
logging.basicConfig(level=LOGLEVEL)

report = os.getenv('REPORT')
if report == '':
    logging.error(
        "No report file specified. Please set the REPORT environment variable."
    )
    exit(1)

def get_metadata():
    metadata = {}
    metadata['title'] = input("title> ") 
    metadata['subtitle'] = input("subtitle> ")
    metadata['author'] = input("author> ") 
    metadata['date'] = input("date> ")
    logging.debug("get_metadata: %s" % metadata)

    logging.info("To avoid input use the config.yaml in out directory")

    return metadata

def process_report():
    logging.info("Asking for metadata")
    metadata = get_metadata()
    header = """---
title: "{}"
author: ["{}"]
date: "{}"
subject: "Markdown"
keywords: [Markdown, Example]
subtitle: "{}"
lang: "fr"
titlepage: true
titlepage-color: "1E90FF"
titlepage-text-color: "FFFAFA"
titlepage-rule-color: "FFFAFA"
titlepage-rule-height: 2
book: true
classoption: oneside
code-block-font-size: \scriptsize
---
    """.format(
            metadata['title'],
            metadata['author'],
            metadata['date'],
            metadata['subtitle']
        )
    logging.debug("header:\n%s" % header)
    logging.info("saving header in out/config.yaml")
    with open('/out/config.yaml', 'w+') as f:
        f.write(header + "\n")
    logging.info("Adding header to the report")
    with open('/tmp/report_processed.md', 'w+') as f:
        f.write(header+"\n")
        with open(report, 'r') as read_f:
            f.write(read_f.read())

def process_report_with(config_file):
    logging.info(f"Adding header from {config_file} to the report")
    with open('/tmp/report_processed.md', 'w+') as f:
        with open(config_file, 'r') as read_f:
            f.write(read_f.read() + "\n")
        with open(report, 'r') as read_f:
            f.write(read_f.read())



if __name__ == '__main__':
    if not(IS_CONFIG_FILE):
        process_report()
    else:
        process_report_with(config_file=PATH_CONFIG)
