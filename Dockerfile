FROM docker.io/debian:latest


RUN apt update && apt install -y \
    pandoc \
    texlive-latex-recommended \
    texlive-fonts-extra \
    texlive-latex-extra \
    p7zip-full \
    wget \
    python3

# create template dir
RUN mkdir -p ~/.local/share/pandoc/templates
ENV EISVOGEL_URL https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/master/eisvogel.tex
RUN wget ${EISVOGEL_URL} -O ~/.local/share/pandoc/templates/eisvogel.latex

# create upload dir
RUN mkdir -p /in
# create output dir
RUN mkdir -p /out

WORKDIR /in

COPY process_report.py /usr/bin/process_report.py

# FIXME: should be env var custom
CMD python3 /usr/bin/process_report.py && \
    echo "Compiling report..." && \
    pandoc /tmp/report_processed.md \
    -o /out/report.pdf \
    --from markdown+yaml_metadata_block+raw_html \
    --template eisvogel \
    --table-of-contents \
    --toc-depth 6 \
    --number-sections \
    --top-level-division=chapter \
    --highlight-style breezedark \
    -V colorlinks=true \
    -V linkcolor=cyan
